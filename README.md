# NoTooNo

NoTooNo (Not Too Noisy) is a sound meter control indicator designed to being used 
in a classroom.

## Getting Started
TO-DO

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project 
on a live system.

### Prerequisites

What things you need to install the software and how to install them

TO-DO

```
Give examples
```

### Installing

TO-DO

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

TO-DO

Explain how to run the automated tests for this system


## Deployment

TO-DO

Add additional notes about how to deploy this on a live system

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc



## Notes
For selecting input and output devices:
https://developers.google.com/web/updates/2015/10/media-devices

Live reload server (no transpilation):
https://medium.com/@svinkle/start-a-local-live-reload-web-server-with-one-command-72f99bc6e855


For testing with http-server:
http-server ./src

Generating audio sources for testing:
https://developer.mozilla.org/es/docs/Web_Audio_API