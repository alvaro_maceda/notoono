const path = require('path');

module.exports = {
    entry: './example.spec.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
};
