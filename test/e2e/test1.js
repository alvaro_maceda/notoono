function sum(a, b) {
    return a * b; // a bug
}

describe('sum', function () {
    it('should return sum of arguments', function () {
        try {
            chai.expect(sum(1, 2)).to.equal(3);
        } catch(e) {
            console.log(e);
            throw e;
        }
    });
});