const { expect, use } = require('chai');
import {RingBuffer} from "../../src/ringBuffer";

describe('ringBuffer', function () {

    it('should store only the latest values', function () {
        let b = new RingBuffer(3);

        b.push('lost');
        b.push('keep1');
        b.push(2);
        b.push(3);

        expect(b.toArray()).to.have.members(['keep1',2,3]);
    });

    it('should return only values aded when less values added than buffer size', function () {
        let b = new RingBuffer(5);
        b.push('value1');
        b.push('value2');
        expect(b.toArray()).to.have.members(['value1', 'value2']);


        b.push(3);
        b.push(4);
        expect(b.toArray()).to.have.members(['value1', 'value2',3,4]);
    });

    it('should return first values first', function () {
        let b = new RingBuffer(3);
        b.push(1);
        b.push(2);
        expect(b.toArray()).to.have.ordered.members([1,2]);

        b.push(3);
        expect(b.toArray()).to.have.ordered.members([1,2,3]);

        b.push(4);
        b.push(5);
        expect(b.toArray()).to.have.ordered.members([3,4,5]);
    });
});