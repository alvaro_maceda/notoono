import {AudioMeterNodeFactory} from "./audio-meter.js";
// import {RingBuffer} from "./ringBuffer";

// Da un error:
// Uncaught (in promise) DOMException: The user aborted a request.
// Parece algo de optimizaciones de chrome:
// https://stackoverflow.com/questions/52760219/use-audioworklet-within-electron-domexception-the-user-aborted-a-request

// If we start audio at onload, it will be stopped. We should start on user interaction
// See autoplay policy:
// https://developers.google.com/web/updates/2017/09/autoplay-policy-changes#webaudio


let audioContext;


document.querySelector('button').addEventListener('click', function() {

    if(!audioContext) {
        audioContext = new AudioContext();
        startVolumeAnalyzer(audioContext).then(()=>console.log('Volume analyzer started'));
        return;
    }

    if(audioContext.state === 'running') {
        console.log('suspending');
        audioContext.suspend().then(function() {
            // Audio suspended
        });
    } else if(audioContext.state === 'suspended') {
        console.log('resumming');
        audioContext.resume().then(function() {
            // Audio resumed
        });
    }
});

async function startVolumeAnalyzer(audioContext) {

    let mikeStream = await openMike();
    let mikeNode = audioContext.createMediaStreamSource(mikeStream);

    let meterNode = await AudioMeterNodeFactory.create(audioContext);
    meterNode.foo('N1');
    //let meterNode2 = await AudioMeterNodeFactory.create(audioContext);
    //meterNode2.foo('N2');

    mikeNode.connect(meterNode);
    //meterNode.connect(meterNode2);
    // Ojo: si no se conecta a la salida, process no se ejecuta
    meterNode.connect(audioContext.destination);
    //meterNode2.connect(audioContext.destination);
}

async function openMike() {

    try {
        let stream = await navigator.mediaDevices.getUserMedia(
            {
                "audio": {
                    "mandatory": {
                        "googEchoCancellation": "false",
                        "googAutoGainControl": "false",
                        "googNoiseSuppression": "false",
                        "googHighpassFilter": "false"
                    },
                    "optional": []
                },
            });
        return stream;
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
}



