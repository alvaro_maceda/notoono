import {createAudioMeter} from './volume-meter.js';
import MyWorker from './volume-analyzer.worker.js';
import {RingBuffer} from "./ringBuffer";
import {prepareCapturing} from "./volume-analyzer.messages";
//https://github.com/webpack-contrib/worker-loader'
//https://www.experoinc.com/post/getting-started-with-web-workers-via-webpack

console.log('Hello, world!');

var audioContext = null;
var meter = null;
var canvasContext = null;
var WIDTH=500;
var HEIGHT=50;
var rafID = null;

window.onload = function() {
    //showVolumeBar();
    createVolumeAnalyzer();
};

let myWorker;

function createVolumeAnalyzer() {
    myWorker = new MyWorker();

    myWorker.onmessage = function (oEvent) {
        console.log(`Called back by the worker!: ${oEvent.data}`);
    };

    // myWorker.postMessage('banana');
    openMike();
}

function openMike() {

    let audioContext = new AudioContext();

    // Attempt to get audio input
    try {
        navigator.mediaDevices.getUserMedia(
            {
                "audio": {
                    "mandatory": {
                        "googEchoCancellation": "false",
                        "googAutoGainControl": "false",
                        "googNoiseSuppression": "false",
                        "googHighpassFilter": "false"
                    },
                    "optional": []
                },
            })
            .then((stream) => gotMike(audioContext,stream))
            .catch(failedGettingStream);
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
}

function failedGettingStream(e){
    console.log(e);
}

function gotMike(audioContext,stream) {
    console.log(`Got mike:${audioContext},${stream}`);
    myWorker.postMessage(prepareCapturing(audioContext,stream));
}





function showVolumeBar() {
    // grab our canvas
    canvasContext = document.getElementById( "meter" ).getContext("2d");

    // grab an audio context
    audioContext = new AudioContext();

    // Attempt to get audio input
    try {
        navigator.mediaDevices.getUserMedia(
            {
                "audio": {
                    "mandatory": {
                        "googEchoCancellation": "false",
                        "googAutoGainControl": "false",
                        "googNoiseSuppression": "false",
                        "googHighpassFilter": "false"
                    },
                    "optional": []
                },
            })
            .then(gotStream)
            .catch(didntGetStream);
    } catch (e) {
        alert('getUserMedia threw exception :' + e);
    }
}

let mediaStreamSource = null;

function didntGetStream() {
    alert('Stream generation failed.');
}

function gotStream(stream) {
    console.log('Got the stream');
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Create a new volume meter and connect it.
    meter = createAudioMeter(audioContext);
    mediaStreamSource.connect(meter);

    // kick off the visual updating
    drawLoop();
}

function drawLoop( time ) {
    // clear the background
    canvasContext.clearRect(0,0,WIDTH,HEIGHT);

    // check if we're currently clipping
    if (meter.checkClipping())
        canvasContext.fillStyle = "red";
    else
        canvasContext.fillStyle = "green";

    // draw a bar based on the current volume
    canvasContext.fillRect(0, 0, meter.volume*WIDTH*1.4, HEIGHT);

    // set up the next visual callback
    rafID = window.requestAnimationFrame( drawLoop );
}