/*
Credit: noiv (https://stackoverflow.com/users/515069/noiv)
 */

function rotate(arr,n) {
    let numRotations = n % arr.length;
    return arr.slice(numRotations, arr.length).concat(arr.slice(0, numRotations));
}

export class RingBuffer {
    constructor(length=1){
        this.pointer = 0;
        this.buffer = [];
        this.length = length;
    };
    get(key){
        return this.buffer[key];
    }
    push(item){
        this.buffer[this.pointer] = item;
        this.pointer = (this.pointer + 1) % this.length;
    }
    toArray(){
        return rotate(this.buffer,this.pointer);
    }
}
