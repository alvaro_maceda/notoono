import {registerMessage, fooMessage} from "./audio-meter.messages.js";

// For webpack (npm start)
// See https://github.com/reklawnos/worklet-loader
//import workletUrl from './audio-meter.worker.js';

// For direct loading (npm run http)
const workletUrl = 'audio-meter.worker.js';

class AudioMeterNode extends AudioWorkletNode {
    constructor(context) {
        super(context, 'audio-meter'); // This must match the call of registerProcessor() in audio-meter.worker.js. Refactor this to a common module.
    }
    foo(text){
        this.port.postMessage(fooMessage(text));
    }
}

export class AudioMeterNodeFactory {

    static async create(context) {
        await context.audioWorklet.addModule(workletUrl);
        return new AudioMeterNode(context);
    }

}
