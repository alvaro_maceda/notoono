function registerMessage(name) {
    return {
        type: registerMessage.TYPE,
        name
    }
}
registerMessage.TYPE = 'REGISTER_MESSAGE';

function fooMessage(foo) {
    return {
        type: fooMessage.TYPE,
        foo
    }
}
fooMessage.TYPE = 'REGISTER_MESSAGE';

export {
    registerMessage,
    fooMessage
};