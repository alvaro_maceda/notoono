class AudioMeterProcessor extends AudioWorkletProcessor {
    constructor() {
        console.log('Constructing myworkletprocessor');
        super();
        this.port.onmessage = this.handleMessage.bind(this);
        this.port.postMessage('OLA K ASE');
    }

    handleMessage(message) {
        console.log(`Mensaje de la página web: ${message.data.type} ${message.data.foo}`);
        this.foo = message.data.foo;
    }

    //https://www.w3.org/TR/webaudio/#audioworkletprocessor
    process(inputs, outputs, parameters) {

        // AudioWorkletGlobalScope data
        console.log(`${this.foo} current time: ${currentTime}`);
        // console.log(`sample rate:${sampleRate}`);

        //console.log(`process. inputs:${inputs.length}`);
        //console.log(`channels: ${inputs[0].length}`);
        // Rendering an audio graph is done in blocks of 128 samples-frames.
        // A block of 128 samples-frames is called a render quantum,
        // and the render quantum size is 128.
        //console.log(`samples: ${inputs[0][0].length}`);


        // The processor may have multiple inputs and outputs. Get the first input and
        // output.
        const input = inputs[0];
        const output = outputs[0];

        // Each input or output may have multiple channels. Get the first channel.
        const inputChannel = input[0];
        const outputChannel = output[0];

        for (let j=0; j< inputChannel.length; ++j) {
            // console.log(inputChannel[j]);
            //this.port.postMessage(inputChannel[j]);
        }

        for (let i = 0; i < outputChannel.length; ++i) {
            // This loop can branch out based on AudioParam array length, but
            // here we took a simple approach for the demonstration purpose.
            outputChannel[i] = 0 //2 * (Math.random() - 0.5);
        }

        /*
          This lifetime policy can support a variety of approaches found in built-in nodes, including the following:

        - Nodes that transform their inputs, and are active only while connected inputs and/or script references exist.
          Such nodes SHOULD return false from process() which allows the presence or absence of connected inputs
          to determine whether active processing occurs.

        - Nodes that transform their inputs, but which remain active for a tail-time after their inputs are disconnected.
          In this case, process() SHOULD return true for some period of time after inputs is found
          to contain zero channels. The current time may be obtained from the global scope’s currentTime
          to measure the start and end of this tail-time interval, or the interval could be calculated
          dynamically depending on the processor’s internal state.

        - Nodes that act as sources of output, typically with a lifetime.
          Such nodes SHOULD return true from process() until the point at which they are no longer producing an output.

          Note that the preceding definition implies that when no return value is provided from an implementation of process(),
          the effect is identical to returning false (since the effective return value is the falsy value undefined).
          This is a reasonable behavior for any AudioWorkletProcessor that is active only when it has active inputs.
        */
        return true;
    }
}

console.log('Registering processor');
registerProcessor('audio-meter', AudioMeterProcessor);

function volumeAudioProcess( event ) {
    var buf = event.inputBuffer.getChannelData(0);
    var bufLength = buf.length;
    var sum = 0;
    var x;

    // Do a root-mean-square on the samples: sum up the squares...
    for (var i=0; i<bufLength; i++) {
        x = buf[i];
        if (Math.abs(x)>=this.clipLevel) {
            this.clipping = true;
            this.lastClip = window.performance.now();
        }
        sum += x * x;
    }

    // ... then take the square root of the sum.
    var rms =  Math.sqrt(sum / bufLength);

    // Now smooth this out with the averaging factor applied
    // to the previous sample - take the max here because we
    // want "fast attack, slow release."
    this.volume = Math.max(rms, this.volume*this.averaging);
}