const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const mainConfig = {

    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        // See webpack issue: https://github.com/webpack/webpack/issues/6642
        globalObject: `(typeof self !== 'undefined' ? self : this)`,
    },

    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                // exclude: [ /node_modules/, /\.worker\.js$/],
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: false }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },

            // If we use this module, we would get an DOM error.
            // It seems to be a bug somewhere in webpack, webpack-dev-server or worker-loader
            // {
            //     test: /\.worker\.js$/,
            //     use: {
            //         loader: 'worker-loader',
            //         // options: { inline: true }
            //         //options: { inline: true, fallback: false }
            //     },
            // }
        ]
    },

    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),

        // We must use this because of the DomError (see worker-loader module)
        new CopyWebpackPlugin([
            {
                from: 'src/**/*worker.js', flatten: true // I use flatter because don't know how to place it in the rigth directory, it creates a src subdirectory
            },
        ])
    ]
};

const audioMeterWorkerConfig = {
    entry: './src/audio-meter.worker.js',
    output: {
        filename: 'audio-meter.worker.js',
        path: path.resolve(__dirname, 'dist'),
        // See webpack issue: https://github.com/webpack/webpack/issues/6642
        globalObject: `(typeof self !== 'undefined' ? self : this)`,
    },
};

// First item should be the main configuration
// Workers configuration should follow
module.exports = [
    mainConfig,
    audioMeterWorkerConfig
];

