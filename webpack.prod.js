const merge = require("webpack-merge");
const commonConfiguration = require("./webpack.common.js");

let [mainConfiguration, ...workersConfiguration] = commonConfiguration;

workersConfiguration = workersConfiguration.map( (configuration) => {
    return merge(configuration, {
        mode: "production"
    });
});

mainConfiguration = merge(mainConfiguration, {
    mode: "production",
});

module.exports = [
    mainConfiguration,
    ...workersConfiguration
];