const path = require("path");
const merge = require("webpack-merge");
const commonConfiguration = require("./webpack.common.js");

let [mainConfiguration, ...workersConfiguration] = commonConfiguration;

workersConfiguration = workersConfiguration.map( (configuration) => {
    return merge(configuration, {
       mode: "development"
    });
});

mainConfiguration = merge(mainConfiguration, {

    mode: "development",
    devtool: "inline-source-map",

    devServer: {
        hot : true,
        inline: true,
        host: '0.0.0.0',
        disableHostCheck: true,
        port: 8081,
        open : false,  // Don't open browser

        // The following two lines are for live reloading workers.
        // See (worker-loader error in webpack.common.js)
        contentBase: path.resolve(__dirname, "src"),
        watchContentBase: true,

        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    },
});

module.exports = [
    mainConfiguration,
    ...workersConfiguration
];

